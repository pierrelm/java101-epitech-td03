package net.epitech.java.td03.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import net.epitech.java.td03.dao.AbstractSmartCardDAO;
import net.epitech.java.td03.exception.SmartCardException;
import net.epitech.java.td03.model.SmartCard;
import net.epitech.java.td03.model.SmartCardType;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.DbUtils;

public class DBUtilSmartCardDAO extends AbstractSmartCardDAO {

	@Override
	public void save(SmartCard card) {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<SmartCard> getSmartCardByType(SmartCardType type)
			throws SmartCardException {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			conn = this.datasource.getConnection();

			stmt = conn
					.prepareStatement("SELECT * FROM SmartCard where type=?");
			stmt.setInt(1, type.getId());
			BeanProcessor processor = new BeanProcessor();
			rs = stmt.executeQuery();
			List<SmartCard> res = processor.toBeanList(rs, SmartCard.class);
			
			return res;

		} catch (SQLException e) {
			throw new SmartCardException(e);
		} finally {

			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(conn);
		}
	}

	@Override
	public Collection<SmartCard> getSmartCardByName(String holderName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(SmartCard card) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

}
