package net.epitech.java.td03.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import net.epitech.java.td03.dao.AbstractSmartCardDAO;
import net.epitech.java.td03.exception.SmartCardException;
import net.epitech.java.td03.model.SmartCard;
import net.epitech.java.td03.model.SmartCardType;
import net.epitech.java.td03.model.querydsl.QSmartCard;

import com.mysema.query.sql.Configuration;
import com.mysema.query.sql.MySQLTemplates;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLTemplates;
import com.mysema.query.sql.dml.SQLInsertClause;
import com.mysema.query.sql.dml.SQLUpdateClause;
import com.mysema.query.types.Projections;

public class QueryDSLSmartCardDAO extends AbstractSmartCardDAO {

	@Override
	public void update(SmartCard card)  throws SmartCardException {
		try (Connection conn = this.datasource.getConnection()) {

			// prepare the query
			SQLTemplates templates = new MySQLTemplates();
			Configuration configuration = new Configuration(templates);

			QSmartCard qcard = QSmartCard.SmartCard;

			SQLUpdateClause clause=new SQLUpdateClause(conn, configuration, qcard);
			clause.where(qcard.id.eq(card.getId()))
			      .set(qcard.type, card.getType())
			      .set(qcard.holdername, card.getHolderName())
			      .execute();
			
			return;

		} catch (SQLException e) {
			throw new SmartCardException(e);
		}
	}

	@Override
	public Collection<SmartCard> getSmartCardByType(SmartCardType type)
			throws SmartCardException {
		try (Connection conn = this.datasource.getConnection()) {

			// prepare the query
			SQLTemplates templates = new MySQLTemplates();
			Configuration configuration = new Configuration(templates);
			SQLQuery query = new SQLQuery(conn, configuration);

			QSmartCard card = QSmartCard.SmartCard;

			// type safe query
			List<SmartCard> cards = query
					.from(card)
					.where(card.type.eq(type.getId()))
					.list(Projections.fields(SmartCard.class, card.id,
							card.holdername, card.type));

			
			return cards;

		} catch (SQLException e) {
			throw new SmartCardException(e);
		}
	}

	@Override
	public Collection<SmartCard> getSmartCardByName(String holderName) throws SmartCardException {
		try (Connection conn = this.datasource.getConnection()) {

			// prepare the query
			SQLTemplates templates = new MySQLTemplates();
			Configuration configuration = new Configuration(templates);
			SQLQuery query = new SQLQuery(conn, configuration);

			QSmartCard card = QSmartCard.SmartCard;

			// type safe query
			List<SmartCard> cards = query
					.from(card)
					.where(card.holdername.eq(holderName))
					.list(Projections.fields(SmartCard.class, card.id,
							card.holdername, card.type));

			
			return cards;

		} catch (SQLException e) {
			throw new SmartCardException(e);
		}
	}

	@Override
	public void save(SmartCard card)  throws SmartCardException {
		try (Connection conn = this.datasource.getConnection()) {

			// prepare the query
			SQLTemplates templates = new MySQLTemplates();
			Configuration configuration = new Configuration(templates);

			QSmartCard qcard = QSmartCard.SmartCard;

			SQLInsertClause clause=new SQLInsertClause(conn, configuration, qcard);
			clause.columns(qcard.type,qcard.holdername).values(card.getType(),card.getHolderName()).execute();
			//card.setId(id);
			
			return;

		} catch (SQLException e) {
			throw new SmartCardException(e);
		}
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

}
