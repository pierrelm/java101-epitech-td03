package net.epitech.java.td03.dao;

import javax.sql.DataSource;

import org.postgresql.ds.PGSimpleDataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import net.epitech.java.td03.dao.impl.DBUtilSmartCardDAO;
import net.epitech.java.td03.dao.impl.QueryDSLSmartCardDAO;
import net.epitech.java.td03.dao.impl.ReflexivitySmartCardDAO;
import net.epitech.java.td03.exception.UnsupportedCodingYearException;

public class SmartCardDAOFactory {

	private DataSource datasource;

	public SmartCardDAOFactory(String user, String password, String serverName,
			String dbName) {
		PGSimpleDataSource ds = new PGSimpleDataSource();
		
		ds.setUser(user);
		ds.setPassword(password);
		ds.setServerName(serverName);
		ds.setDatabaseName(dbName);
		this.datasource = ds;

	}

	/**
	 * 
	 * @param year
	 *            year nunber on 4 digits
	 * @return
	 * @throws UnsupportedCodingYearException
	 */
	public SmartCardDAO getDaoByYearOfCoding(Integer year)
			throws UnsupportedCodingYearException {
		AbstractSmartCardDAO dao = null;
		if (year.equals(2003)) {
			dao = new ReflexivitySmartCardDAO();
		} else if (year.equals(2005)) {
			dao = new DBUtilSmartCardDAO();
		} else if (year.equals(2011)) {
			dao = new QueryDSLSmartCardDAO();
		} else {
			throw new UnsupportedCodingYearException();
		}

		dao.setDatasource(datasource);
		return dao;
	}
}
