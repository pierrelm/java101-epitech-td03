package net.epitech.java.td03.model;

import javax.annotation.Generated;

import net.epitech.java.td03.annotation.DBTable;

/**
 * SmartCard is a Querydsl bean type
 */
@Generated("com.mysema.query.codegen.BeanSerializer")
@DBTable(name="SmartCard")
public class SmartCard {

	
	private String holdername;

	private Integer id;

	private Integer type;

	public String getHolderName() {
		return holdername;
	}

	public void setHoldername(String holdername) {
		this.holdername = holdername;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "SmartCard [holdername=" + holdername + ", id=" + id + ", type="
				+ type + "]";
	}
}
