package net.epitech.java.td03.dao;

import java.util.Collection;

import net.epitech.java.td03.exception.SmartCardException;
import net.epitech.java.td03.model.SmartCard;
import net.epitech.java.td03.model.SmartCardType;

public interface SmartCardDAO {

	// CREATE
	/**
	 * persist a new smartCard to Database
	 * 
	 * @param card
	 * @throws SmartCardException 
	 */
	public void save(SmartCard card) throws SmartCardException;

	/**
	 * retreive persisted Smartcard for a given type
	 * 
	 * @param type
	 *            a smartcard type
	 * @return a collection of Smartcard corresponding to the supplied type
	 * @throws SmartCardException if an error occures
	 */
	public Collection<SmartCard> getSmartCardByType(SmartCardType type) throws SmartCardException;

	/**
	 * retreive persisted Smartcard for a giver holder name
	 * 
	 * @param holderName
	 *            full name of the holder
	 * @return a collection of Smartcard corresponding to the supplied holder
	 *         Name
	 * @throws SmartCardException 
	 */
	public Collection<SmartCard> getSmartCardByName(String holderName) throws SmartCardException;

	/**
	 * for a currently persisted Smartcard, update the DB reccord
	 * 
	 * @param card
	 *            the smartcard object to be updated
	 * @throws SmartCardException 
	 */
	public void update(SmartCard card) throws SmartCardException;

	/**
	 * Delete a smartcard from the database
	 * 
	 * @param id
	 */
	public void delete(int id) throws SmartCardException;

}
