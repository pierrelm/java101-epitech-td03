package net.epitech.java.td01.service;

import static org.junit.Assert.*;

import java.util.Collection;

import net.epitech.java.td01.model.Course;
import net.epitech.java.td01.model.Teacher;
import net.epitech.java.td01.service.contract.Service;
import net.epitech.java.td01.service.impl.Pair;
import net.epitech.java.td01.service.impl.ServiceImpl;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testOverlap() {
		Service service = new ServiceImpl();
		Course course= new Course();
		course.setName("JAVA");
		course.setDate(new DateTime());
		course.setDuration(new Duration(1000*3600*3));
		
		service.addCourse(course);
		try{
			service.addCourse(course);
			//KO
			fail("should have thrown");
		}
		catch(Exception e){
			//OK
		}
	}
	
	@Test
	public void testDeleteCourse()
	{
		Service s = new ServiceImpl();
		Course c = new Course();
		
		try{
			s.deleteCourse(c);
			//KO
			fail("should have thrown");
		}
		catch(Exception e){
			//OK
		}
	}
	
	@Test
	public void testAvailableSlots()
	{
		Service s = new ServiceImpl();
		Collection<Pair<DateTime, DateTime>> slots = s.getAvaibleTimeSlot(new Duration(1000 * 3600));

		if (slots.size() == 0)
			fail("should have slots");
	}

}
