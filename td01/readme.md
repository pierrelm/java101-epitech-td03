# TD1 : MVC et Spring

## Objectifs du TD

### Rappels
+ Récap du TD00, TD0
+ Fondement du JavaWeb, ma première servlet
+ Le nouveau new: Présentation de l'IOC et injection de dépendance

### Techniques
+ travail collaboratif pendant le TD
+ Spring et l''IOC
+ Présentation du pattern Model-View-Controler
+ Implémentation du MVC Avec Spring

## TD01


### Travail collaborarif pendant le TD
+ notre base commune de code: java101-epitech
+ créez votre compte bitbucket
+ allez sur la page du projet java101-epitech ( https://bitbucket.org/mirlitone/java101-epitech)
+ cliquez sur Fork
+ clonnez votre nouveau repository en local
+ editez les fichier dans td01/td01-mvc-hands-on
+ pusher dans votre repository forké
+ demander une pull request de votre repository (master) vers le repository  mirlitone/java101-epitech (master) et attendez le résultat de la code review

### Le MVC


* A controller can send commands to the model to update the model's state (e.g., editing a document). It can also send commands to its associated view to change the view's presentation of the model (e.g., by scrolling through a document).
* A model notifies its associated views and controllers when there has been a change in its state. This notification allows the views to produce updated output, and the controllers to change the available set of commands. In some cases an MVC implementation might instead be "passive," so that other components must poll the model for updates rather than being notified.
* A view requests information from the model that it needs for generating an output representation to the user


### Spring et l'IOC

+ sources: java101-epitech/td01/td01-mvc
+ la dernière fois, nous avons réalisé l'IOC avec Guice (faile à configurer)
+ Spring et le n°1 des framework MVC Java
+ Spring et Guice respectent la norme JSR-330: en théorie seule la configuration change
+ historiquement la configuration se fait avec un fichier applicationContext.xml [http://docs.spring.io/spring/docs/3.2.6.RELEASE/spring-framework-reference/htmlsingle/]
+ ici, la configuration est basée sur des annotations dans net.epitech.java.td01.config.MvcConfiguration
+ le web.xml est plus complexe qu'au TD0 (voir les commentaires ajoutés en ligne)

### hands on!

* afficher un page HTML avec tous les cours de 3ième année
* Pour chaque cours, un prof est associé
* afficher une page avec tous les profs et les cours correspondants
* un prof= nom et adresse mail
* un prof peut donner plusieurs cours




