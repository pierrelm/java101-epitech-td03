package net.epitech.java.td.exception;

/**
 * execption used when we failed to parse a full text user entered day of the week.
 * @author nicolas
 *
 */
public class FailedToParseDayInWeekException extends Exception {

}
