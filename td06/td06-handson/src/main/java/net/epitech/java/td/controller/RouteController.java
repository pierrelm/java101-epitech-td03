package net.epitech.java.td.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import net.epitech.java.td.controller.form.CourseFormElement;
import net.epitech.java.td.controller.form.TeacherFormElement;
import net.epitech.java.td.controller.form.UserChangePasswordForm;
import net.epitech.java.td.domain.Course;
import net.epitech.java.td.domain.Teacher;
import net.epitech.java.td.exception.FailedToParseDayInWeekException;
import net.epitech.java.td.exception.FailedToParseMailException;
import net.epitech.java.td.exception.InvalidPasswordException;
import net.epitech.java.td.exception.NoSuchCourseException;
import net.epitech.java.td.exception.NoSuchTeacherException;
import net.epitech.java.td.service.EpitechService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Main controller of the application.
 * @author nicolas
 *
 */
@Controller
public class RouteController {

	@Inject
	private EpitechService service;

	@RequestMapping(value = { "/secure/submit-password-change" }, method = { RequestMethod.POST })
	public String submitPasswordChange(
			@Valid @ModelAttribute("changePass") UserChangePasswordForm userChangePasswordForm,
			BindingResult result, Principal principal) {

		if (!userChangePasswordForm.getConfirmation().equals(
				userChangePasswordForm.getNewPassword())) {
			result.rejectValue("newPassword", "newPassword",
					"Password mismatch");
			result.rejectValue("confirmation", "confirmation",
					"Password mismatch");
		}

		try {
			if (!result.hasErrors()) {
				service.changePassword(principal.getName(),
						userChangePasswordForm.getNewPassword(),

						userChangePasswordForm.getOldPassword());

				return "password-successfully-changed";
			}
			;

		} catch (NoSuchTeacherException e) {
			return "redirect:logout";
		} catch (InvalidPasswordException e) {
			result.rejectValue("oldPassword", "oldPassword",
					"Old Password doesn't match");

		}

		return "password-change";

	}

	@RequestMapping(value = { "/secure/my-account" }, method = { RequestMethod.GET })
	public String getMyAccount(ModelMap model, Principal principal)
			throws IOException {
		UserChangePasswordForm userChangePasswordForm = new UserChangePasswordForm();
		model.put("changePass", userChangePasswordForm);

		return "password-change";
	}

	@RequestMapping(value = { "/cours", "/" }, method = { RequestMethod.GET })
	public String getCourses(ModelMap model) throws IOException {

		model.addAttribute("courses", service.getCourses());
		return "courses";
	}

	@RequestMapping(value = { "/detach-course-from-teacher" }, method = { RequestMethod.GET })
	public ResponseEntity<String> detachCourseFromTeacher(
			@RequestParam("courseId") Integer courseId,
			@RequestParam("teacherId") Integer teacherId) {
		try {
			service.detachCourseFromTeacher(courseId, teacherId);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (NoSuchTeacherException | NoSuchCourseException e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = { "/delete-course" }, method = { RequestMethod.GET })
	public String deleteCourse(ModelMap model,
			@RequestParam("courseId") Integer courseId) {

		Course course;
		try {
			course = service.getCourseById(courseId);
			model.put("course", course);
			return "delete-course-confirm";
		} catch (NoSuchCourseException e) {
			model.put("error", e.getMessage());
			return "error";
		}

	}

	@RequestMapping(value = { "/delete-course-confirmed" }, method = { RequestMethod.GET })
	public String deleteCourseConfirmed(ModelMap model,
			@RequestParam("courseId") Integer courseId) {

		try {
			service.deleteCourse(courseId);
		} catch (NoSuchCourseException e) {
			model.put("error", "course already deleted");
			return "error";
		}
		return "redirect:/";

	}

	@RequestMapping(value = { "/teachers" }, method = { RequestMethod.GET })
	public String getTeachers(ModelMap model) throws IOException {

		model.addAttribute("teachers", service.getTeachers());
		return "teachers";
	}

	@RequestMapping(value = { "/unemployed-teachers" }, method = { RequestMethod.GET })
	public String getUnemployedTeachers(ModelMap model) throws IOException {

		model.addAttribute("teachers", service.getTeacherWithNoCourses());
		return "teachers";
	}

	@Transactional
	@RequestMapping(value = "/teacher/{id}")
	public String coursesProfessor(ModelMap model,
			@PathVariable("id") Integer idTeacher) throws IOException {

		Teacher theone = service.getTeacher(idTeacher);
		model.addAttribute("teacher", theone);
		List<Course> courses = theone.getCourses();
		model.addAttribute("courses", courses);
		return "teacher";
	}

	@RequestMapping(value = "/nouveau-cours")
	public String addNewCourses(ModelMap model) throws IOException {

		model.addAttribute("course", new CourseFormElement());
		return "newcourse";
	}

	@RequestMapping(value = "/nouveau-teacher")
	public String addNewTeachers(ModelMap model) throws IOException {

		model.addAttribute("teacher", new TeacherFormElement());
		model.addAttribute("courses", service.getCourses());
		return "newteacher";
	}

	@RequestMapping(value = "/teacher", method = { RequestMethod.POST })
	public String addNewTeacher(
			@Valid @ModelAttribute("teacher") TeacherFormElement teacher,
			BindingResult result, HttpServletRequest request)
			throws IOException {

		if (result.hasErrors()) {
			return "newteacher";
		}

		Integer coursID = null;
		if (teacher.getCours() != null && !"NONE".equals(teacher.getCours())) {
			coursID = Integer.parseInt(teacher.getCours());
		}

		// TODO: pass those to service layer to create course element
		try {
			service.addTeacherWithCourse(teacher.getName(), teacher.getEmail(),
					teacher.getPassword(), coursID);
		} catch (FailedToParseMailException e) {
			result.rejectValue("email", "bademail", "This teacher exists");
			return "newteacher";
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/course", method = { RequestMethod.POST })
	public String addNewCourse(
			@Valid @ModelAttribute("course") CourseFormElement course,
			BindingResult result, HttpServletRequest request,
			Principal principal) throws IOException {

		if (result.hasErrors()) {
			return "newcourse";
		}

		try {
			service.addCourse(course.getName(), course.getDayInweek(),
					course.getDuration(), principal.getName());
		} catch (FailedToParseDayInWeekException e) {
			result.rejectValue("dayInweek", "baddayInweek",
					"Failed to parse Day in Week");
			return "newcourse";
		}

		return "redirect:/";
	}

	@RequestMapping(value = "/test")
	public ModelAndView newCourse(HttpServletResponse response)
			throws IOException {

		return new ModelAndView("test", "command", new Course());
	}

	@RequestMapping("/planning")
	public String getPlanning(ModelMap model) throws IOException {

		model.addAttribute("courses", service.getCourses());

		return "planning";
	}
}
