package net.epitech.java.td.exception;
/**
 * throw when no such teacher exist in the system.
 * @author nicolas
 *
 */
public class NoSuchTeacherException extends Exception {

	public NoSuchTeacherException(String message) {
		super(message);
	}

}
