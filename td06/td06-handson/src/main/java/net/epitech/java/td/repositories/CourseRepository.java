package net.epitech.java.td.repositories;

import java.util.List;

import net.epitech.java.td.domain.Course;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * interface implemented by spring data. Allow access for courses.
 * @author nicolas
 *
 */
public interface CourseRepository extends JpaRepository<Course, Integer>,
		QueryDslPredicateExecutor<Course> {

	public List<Course> findCourseByName(String name);
}
