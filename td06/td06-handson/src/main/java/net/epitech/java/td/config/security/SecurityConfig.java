package net.epitech.java.td.config.security;

import java.util.Arrays;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * This class is here to configure everythin about security, it's automatically
 * included by @EnableWebSecurity and package scanning
 * 
 * @author nicolas
 * 
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * this bean encapsulate all the method to authenticate (here we have just
	 * one)
	 * 
	 * @param dbProvider
	 * @return
	 */
	@Inject
	@Bean
	public ProviderManager getAuthenticationProviderManager(
			AuthenticationProvider dbProvider) {

		ProviderManager providerMgr = new ProviderManager(
				Arrays.asList(dbProvider));
		return providerMgr;

	}

	/**
	 * provide a secure way to hash passwords (salted)
	 * 
	 * @return
	 */
	@Bean
	public PasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	// plugs the userDatailService with the DAOAuthenticationProvider
	@Inject
	@Bean
	public AuthenticationProvider getDBAuthenticationProvider(
			UserDetailsService userDetailsService, PasswordEncoder encoder) {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setPasswordEncoder(encoder);
		provider.setUserDetailsService(userDetailsService);
		return provider;

	}

	// configure what role is allowed to display which url
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/resources/**").permitAll()
				.antMatchers("/teacher**").hasAuthority("ADMIN")
				.antMatchers("/cours**").permitAll().anyRequest()
				.authenticated().and().formLogin();

	}
}
