package net.epitech.java.td.exception;


public class InvalidPasswordException extends Exception {

	public InvalidPasswordException(String string) {
		super(string);
	}

}
